import MatrixImplicits._
import model.{Matrix2, Vector2}

object VectorImplicits {

  implicit class ArithmeticVectorImplicits(v: Vector2) {

    def +(v2: Vector2): Vector2 = ???

    def *(scalar: Float): Vector2 = ???

    def *(m: Matrix2): Vector2 = ???

    def isLinearlyDependent(v2: Vector2): Boolean = ???

  }

}
