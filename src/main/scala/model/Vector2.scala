package model

case class Vector2(
                    xScalar: Float,
                    yScalar: Float
                  )
