import VectorImplicits._
import model.{Matrix2, Vector2}

object MatrixImplicits {

  implicit class ArithmeticMatrixImplicits(m: Matrix2) {

    def *(m2: Matrix2): Matrix2 = ???

    def *(scalar: Float): Matrix2 = ???

    def determinant(): Float = ???

    def changesOrientation(): Boolean = ???

    def inverse(): Option[Matrix2] = ???

  }

}
