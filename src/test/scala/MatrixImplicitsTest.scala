import MatrixImplicits._
import model.{Matrix2, Vector2}
import org.scalatest.{Matchers, WordSpec}

class MatrixImplicitsTest extends WordSpec with Matchers {

  "MatrixImplicits" when {
    "multiplying Matrix2.rotate90clockwise with itself" should {
      "equal Matrix2.rotate180clockwise" in {
        val composition = Matrix2.rotate90clockwise * Matrix2.rotate90clockwise

        composition should be(Matrix2.rotate180clockwise)
      }
    }

    "calculating the determinant" when {
      "the matrix is an identity, rotation, or sheer matrix" should {
        "return 1" in {
          Matrix2.identity.determinant() should be(1)
          Matrix2.rotate90clockwise.determinant() should be(1)
          Matrix2.rotate180clockwise.determinant() should be(1)
          Matrix2.sheerRight(0.2f).determinant() should be(1)
          Matrix2.sheerRight(1.1f).determinant() should be(1)
          Matrix2.sheerRight(-1.1f).determinant() should be(1)
        }
      }

      "the matrix scales by the factor of 3" should {
        "return 9" in {
          Matrix2.scale(3).determinant() should be(9)
        }
      }
    }

    "calling changesOrientation" when {
      "a matrix flips the orientation" should {
        "return true" in {
          Matrix2(
            Vector2(1, 0),
            Vector2(0, -1)
          ).changesOrientation() should be(true)
        }
      }

      "a matrix retains the orientation" should {
        "return false" in {
          Matrix2.identity.changesOrientation() should be(false)
          Matrix2.rotate90clockwise.changesOrientation() should be(false)
          Matrix2.rotate180clockwise.changesOrientation() should be(false)
          Matrix2.scale(2.3f).changesOrientation() should be(false)
          Matrix2.sheerRight(3.2f).changesOrientation() should be(false)
        }
      }
    }

    "calculating the inverse" when {
      "the matrix is full rank" should {
        "satisfy the properties of an invertible matrix" in {
          Matrix2.rotate90clockwise * Matrix2.rotate90clockwise.inverse().get == Matrix2.identity should be(true)
          Matrix2.rotate90clockwise.inverse().get * Matrix2.rotate90clockwise == Matrix2.identity should be(true)
          Matrix2(
            Vector2(0, 1),
            Vector2(-1, 0)
          ) == Matrix2.rotate90clockwise.inverse().get should be(true)
        }
      }

      "the matrix is not full rank" should {
        "return None" in {
          Matrix2(
            Vector2(0, 0),
            Vector2(0, 0)
          ).inverse().isEmpty should be(true)
        }
      }
    }
  }
}
