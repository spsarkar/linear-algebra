import VectorImplicits._
import model.{Matrix2, Vector2}
import org.scalatest.{Matchers, WordSpec}

class VectorImplicitsTest extends WordSpec with Matchers {

  "ArithmeticVectorImplicits" when {
    "multiplying a vector with Matrix2.rotate90clockwise" should {
      "rotate the vector 90 degrees" in {
        val vector = Vector2(1.2f, -0.8f)

        val newVector = vector * Matrix2.rotate90clockwise

        newVector.xScalar should be(-0.8f)
        newVector.yScalar should be(-1.2f)
      }
    }

    "multiplying a vector with Matrix2.rotate90clockwise four times" should {
      "return the same vector" in {
        val vector = Vector2(1.2f, -0.8f)

        val newVector = vector * Matrix2.rotate90clockwise * Matrix2.rotate90clockwise * Matrix2.rotate90clockwise * Matrix2.rotate90clockwise

        newVector should be(vector)
      }
    }

    "isLinearlyDependent is called" when {
      val vector = Vector2(1, 1)

      "two vectors are linearly dependent" should {
        val dependentVector = Vector2(3.3f, 3.3f)
        "return true" in {
          vector.isLinearlyDependent(dependentVector) should be(true)
        }
      }

      "isLinearlyDependent is called on two linearly independent vectors" should {
        val independentVector = Vector2(3.1f, 3.3f)
        "return false" in {
          vector.isLinearlyDependent(independentVector) should be(false)
        }
      }
    }
  }
}
